#!/usr/bin/env python

"""Tests for `mariqt_widgets` package."""

import pytest
import os
import pandas as pd
import json
from pprint import pprint
from deepdiff import DeepDiff
from freezegun import freeze_time

import mariqt.core as miqtc
from mariqt_widgets import notebook_widgets
from mariqt_widgets import ifdo_excel
from mariqt_widgets import ifdo_create


TEST_BASE_DIR = os.path.dirname(os.path.abspath(__file__))


@pytest.fixture
def response():
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    # import requests
    # return requests.get('https://github.com/audreyr/cookiecutter-pypackage')


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument."""
    # assert 'GitHub' in response.text


def test_icons():

    icon = notebook_widgets.ICON_CHECK
    icon = notebook_widgets.ICON_CHECKDISABLED
    icon = notebook_widgets.ICON_ERROR
    icon = notebook_widgets.ICON_ERRORDISABLED
    icon = notebook_widgets.ICON_WARNING

@freeze_time("2025-01-01", auto_tick_seconds=15)
def test_ifdo_export_to_excel():

    output_file = TEST_BASE_DIR + '/test_files/excel_export/iFDO_export_test.xlsx'
    input_dir = TEST_BASE_DIR + '/test_files/ifdos'
    ignore_sub_dirs = ['ignored']
    filter = {}
    output_fields = [   'image-set-name',
                        'image-context',
                        'image-project',
                        'image-event',
                        'image-pi',
                        'image-license',
                        'image-copyright',
                        'image-abstract',
                        'image-objective',
                        'image-target-environment',
                        'image-spatial-constraints',
                        'image-target-timescale',
                        'image-temporal-constraints',
                        'image-deployment',
                        'image-marine-zone',
                        'image-average-color'
                    ]
    ifdo_excel.ifdo_export_to_excel(output_file,input_dir,ignore_sub_dirs,filter,output_fields)

    excel = pd.ExcelFile(output_file)
    excel_compare = pd.ExcelFile(output_file.replace('_test.xlsx','_compare.xlsx'))
    for sheet in excel.sheet_names:
        df = excel.parse(sheet)
        df_compare = excel_compare.parse(sheet)

        assert df.equals(df_compare)


def test_ifdo_update_from_excel():

    input_excel_file = TEST_BASE_DIR + '/test_files/excel_export/iFDO_export_compare.xlsx'
    ifdo_file = TEST_BASE_DIR + '/test_files/ifdo_update/products/input.json'
    sheet_name = "Stations"

    ifdo_excel.ifdo_update_from_excel(input_excel_file, ifdo_file, sheet_name)

    ifdo_file = TEST_BASE_DIR + '/test_files/ifdo_update/products/MSM96_003_AUV-01_GMR_CAM-1_iFDO.json'
    ifdo_file_compare = TEST_BASE_DIR + '/test_files/ifdo_update/products/MSM96_003_AUV-01_GMR_CAM-1_iFDO.json_compare'
    hash = miqtc.sha256HashFile(ifdo_file)
    hash_compare = miqtc.sha256HashFile(ifdo_file_compare)
    if not hash == hash_compare:
        o = open(ifdo_file, 'r')
        ifdo_file_dct = json.load(o)
        o.close() 
        o = open(ifdo_file_compare, 'r')
        ifdo_file_compare_dct = json.load(o)
        o.close() 
        diff = DeepDiff(ifdo_file_compare_dct,ifdo_file_dct)
        pprint(diff)
        raise Exception(diff)
    assert hash == hash_compare

    #tear down
    protocolPath = TEST_BASE_DIR + '/test_files/ifdo_update/protocol/'
    prov_files = [f for f in os.listdir(protocolPath) if f.split('.')[-1] == "yaml"]
    for file in prov_files:
        os.remove(os.path.join(protocolPath,file))


def test_ifdo_create_get_min_header_fields():
    ret = ifdo_create.get_min_header_fields()


def test_convert_excel_markdown():
    input_md_file = TEST_BASE_DIR + '/test_files/convert_excel_markdown/iFDO_export_compare.md'
    hash_start = miqtc.sha256HashFile(input_md_file)
    # convert markdown to excel
    excel_file = ifdo_excel.markdown_to_excel(input_md_file, overwrite=True)
    
    # convert excel back to markdown
    md_file = ifdo_excel.excel_to_markdown(excel_file, overwrite=True)

    hash_end = miqtc.sha256HashFile(md_file)

    # clean up
    os.remove(excel_file)

    assert hash_start == hash_end

def test_get_allowed_values():
    ifdo_field_property = {
                    "description": "The overarching project context within which the image set was created",
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string",
                            "description": "The name of the context"
                        },
                        "uri": {
                            "type": "string",
                            "format": "uri",
                            "description": "A URI pointing to details of the context"
                        }
                    },
                    "required": ["name"]
                }
    allow_values = ifdo_excel._get_allowed_values(ifdo_field_property)
    assert allow_values == "dict {name: string, uri: string(format: uri)}"

    ifdo_field_property = {
                    "description": "Copyright statement or contact person or office",
                    "type": "string"
                }
    allow_values = ifdo_excel._get_allowed_values(ifdo_field_property)
    assert allow_values == "string"

    ifdo_field_property = {
                    "description": "mapping: planned path execution along 2-3 spatial axes, stationary: fixed spatial position, survey: planned path execution along free path, exploration: unplanned path execution, experiment: observation of manipulated environment, sampling: ex-situ imaging of samples taken by other method",
                    "type": "string",
                    "enum": [
                        "mapping",
                        "stationary",
                        "survey",
                        "exploration",
                        "experiment",
                        "sampling"
                    ]
                }
    allow_values = ifdo_excel._get_allowed_values(ifdo_field_property)
    assert allow_values == "['mapping', 'stationary', 'survey', 'exploration', 'experiment', 'sampling']"

    ifdo_field_property = {
                    "description": "The average colour for each image / frame and the n channels of an image (e.g. 3 for RGB)",
                    "type": "array",
                    "minItems": 3,
                    "maxItems": 3,
                    "items": {
                        "type": "integer",
                        "minimum": 0,
                        "maximum": 256
                    }
                }
    allow_values = ifdo_excel._get_allowed_values(ifdo_field_property)
    assert allow_values == "array(minItems: 3, maxItems: 3, items: {'type': 'integer', 'minimum': 0, 'maximum': 256})"
