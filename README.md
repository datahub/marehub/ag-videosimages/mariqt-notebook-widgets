# mariqt_widgets 

Contains widgets commonly used by MarIQT Notebooks.

 * Free software: GNU General Public License v3 
 * Documentation:  https://datahub.pages.hzdr.de/marehub/ag-videosimages/mariqt-notebook-widgets/


# Features

-   TODO

# Unittests

run with `pytest`

# Credits

This package was created with
[Cookiecutter](https://github.com/audreyr/cookiecutter) and the
[open-source/toolbox/cookiecutter-pypackage](https://git.geomar.de/open-source/toolbox/cookiecutter-pypackage)
project template.
