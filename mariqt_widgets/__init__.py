"""Top-level package for mariqt_widgets."""

__author__ = """Karl Heger"""
__email__ = 'kheger@geomar.de'
__version__ = '0.4.2'
