#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.md') as history_file:
    history = history_file.read()

requirements = ['mariqt','ipywidgets', 'ipyfilechooser', 'markdown']

test_requirements = ['pytest>=3', ]

setup(
    author="Karl Heger",
    author_email='kheger@geomar.de',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Contains widgets commonly used by MarIQT Notebooks.",
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='mariqt_widgets',
    name='mariqt_widgets',
    packages=find_packages(include=['mariqt_widgets', 'mariqt_widgets.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://codebase.helmholtz.cloud/datahub/marehub/ag-videosimages/mariqt-notebook-widgets',
    version='0.4.2',
    zip_safe=False,
)

