# History

0.4.0

- updated for mariqt v1.0.0 

0.3.8

- updated for mariqt v0.6.9

0.3.8

- exceptions while trying to get info from osis are now caught
- fixed tests for current iFDO version

0.1.0 (2023-05-09)


-   First release 
